create database form;
use form;

CREATE TABLE company(
    id int Primary Key Auto_Increment Not Null,
    full_name VARCHAR(100) UNIQUE,
    training VARCHAR(100),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS question(
    id int Primary Key Auto_Increment Not Null,
    quote TEXT,
    rating_id int, Foreign Key(rating_id) References rating(id),
    yesno ENUM('YES','NO'),
    company_id int, Foreign Key(company_id) References company(id)
);

CREATE TABLE IF NOT EXISTS rating(
    id int Primary Key Auto_Increment Not Null,
    description TEXT
);

INSERT INTO rating(id, description)VALUES
(1, 'Strongly Disagree'),
(2, 'Disagree'),
(3, 'Neutral'),
(4, 'Agree'),
(5, 'Strongly Agree');


